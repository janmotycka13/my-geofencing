package uhk.cz.mygeofencing.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.security.Provider;
import java.util.ArrayList;
import java.util.List;

import uhk.cz.mygeofencing.R;
import uhk.cz.mygeofencing.model.GeofenceRepository;
import uhk.cz.mygeofencing.model.LocationService;
import uhk.cz.mygeofencing.model.SimpleGeofence;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener {

    LocationManager locationManager;
    LocationListener locationListener;
    GoogleMap googleMap;
    Marker marker, geofenceMarker;
    Circle geofenceLimits;
    Location current;
    final static int MENU_ITEM_ADD = 1, MENU_ITEM_CLEAR = 2, MENU_ITEM_SHOW = 3, MENU_ITEM_SETTINGS = 4;
    public final static int REQUEST_LOCATION_UPDATES = 999;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                LatLng position = new LatLng(location.getLatitude(), location.getLongitude());
                MarkerOptions markerOptions = new MarkerOptions().position(position);

                if (marker != null) {
                    marker.remove();
                }

                marker = googleMap.addMarker(markerOptions);

                if (geofenceMarker == null) googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 14f));
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            }
        };

        requestLocationUpdates();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent i = new Intent(getApplicationContext(), LocationService.class);
        startService(i);
    }

    private void requestLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(MainActivity.this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{ACCESS_FINE_LOCATION}, 10);
            }
            return;
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, REQUEST_LOCATION_UPDATES, 0, locationListener);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 10) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(MainActivity.this, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    Intent i = new Intent(getApplicationContext(), LocationService.class);
                    stopService(i);
                    startService(i);

                    if (locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null) {
                        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        addMarker(latLng);
                    }

                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, REQUEST_LOCATION_UPDATES, 0, locationListener);
                }
            } else {
                ArrayList<String> result = new ArrayList<>();

                AlertDialog.Builder dlg = new AlertDialog.Builder(getApplicationContext());
                dlg.create();
                dlg.setTitle(R.string.title_error);

                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) result.add(permissions[i]);
                }

                if (result.size() > 0) {
                    dlg.setMessage(getApplicationContext().getString(R.string.msg_required_permission, TextUtils.join(", ", result)));
                    dlg.show();
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setOnMapClickListener(this);
        this.googleMap.getUiSettings().setZoomControlsEnabled(true);

        if (ActivityCompat.checkSelfPermission(MainActivity.this, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null) {
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            addMarker(latLng);
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if (geofenceMarker != null) geofenceMarker.remove();

        String title = String.format("%s, %s", latLng.latitude, latLng.longitude);
        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .title(title);

        geofenceMarker = googleMap.addMarker(options);

        drawGeofence();
    }

    private void addMarker(LatLng position) {
        MarkerOptions markerOptions = new MarkerOptions().position(position);

        if (marker != null) {
            marker.remove();
        }

        marker = googleMap.addMarker(markerOptions);

        if (geofenceMarker == null) googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 14f));
    }

    private void drawGeofence() {
        if (geofenceLimits != null) geofenceLimits.remove();

        CircleOptions circleOptions = new CircleOptions()
                .center(geofenceMarker.getPosition())
                .strokeColor(Color.argb(50, 70,70,70))
                .fillColor( Color.argb(100, 150,150,150) )
                .radius(GeofenceRepository.RADIUS);

        geofenceLimits = googleMap.addCircle(circleOptions);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(1, MENU_ITEM_ADD, MENU_ITEM_ADD, getString(R.string.menu_add));
        menu.add(1, MENU_ITEM_CLEAR, MENU_ITEM_CLEAR, getString(R.string.menu_clear));
        menu.add(1, MENU_ITEM_SHOW, MENU_ITEM_SHOW, getString(R.string.menu_show));
        menu.add(1, MENU_ITEM_SETTINGS, MENU_ITEM_SETTINGS, getString(R.string.menu_settings));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Context context = getApplicationContext();
        final GeofenceRepository repository = new GeofenceRepository(context);
        switch (item.getItemId()) {
            case MENU_ITEM_ADD:
                if (geofenceMarker == null) break;
                repository.addGeofence(new SimpleGeofence(geofenceMarker.getPosition().latitude, geofenceMarker.getPosition().longitude, GeofenceRepository.RADIUS));
                Toast.makeText(context, getString(R.string.msg_add), Toast.LENGTH_SHORT).show();
                break;
            case MENU_ITEM_CLEAR:
                repository.clear();
                Toast.makeText(context, getString(R.string.msg_clear), Toast.LENGTH_SHORT).show();
                break;
            case MENU_ITEM_SHOW:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.title_dlg_geofences);
                List<SimpleGeofence> geofences = repository.getGeofences();

                String msg = "";

                for (SimpleGeofence g : geofences) {
                    msg += g.toString() + "\n\n";
                }

                builder.setMessage(msg);

                AlertDialog dialog = builder.create();
                dialog.show();

                break;
            case MENU_ITEM_SETTINGS:
            final CharSequence[] items = {" Accelerometer "};
            final boolean[] selectedItems = {repository.getAccelerometer()};
            AlertDialog d = new AlertDialog.Builder(this)
            .setTitle("Settings")
            .setMultiChoiceItems(items, selectedItems, new DialogInterface.OnMultiChoiceClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
                    if (indexSelected == 0) {
                        repository.setAccelerometer(isChecked);
                        Intent i = new Intent(getApplicationContext(), LocationService.class);
                        stopService(i);
                        startService(i);
                    }
                }
            }).create();
        d.show();

                break;
        }

        return super.onOptionsItemSelected(item);
    }
}