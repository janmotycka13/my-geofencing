package uhk.cz.mygeofencing.model;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;


public class GeofenceLocationListener implements LocationListener {

    Context context;

    public GeofenceLocationListener(Context context) {
        this.context = context;
    }

    @Override
    public void onLocationChanged(Location location) {
        List<SimpleGeofence> geofences = new GeofenceRepository(context).getGeofences();
        for (int i = 0; i < geofences.size(); i++) {
            SimpleGeofence g = geofences.get(i);
            if (GeofenceUtil.checkInside(g, new LatLng(location.getLatitude(), location.getLongitude()))) {
                new GeofenceRepository(context).removeGeofence(i);
                Intent in = new Intent("location_update");
                in.putExtra("coordinates", location.getLongitude() + " " + location.getLatitude());
                context.sendBroadcast(in);
            }
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

}
