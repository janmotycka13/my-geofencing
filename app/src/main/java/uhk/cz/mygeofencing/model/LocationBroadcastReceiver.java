package uhk.cz.mygeofencing.model;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;

import java.util.Random;

import uhk.cz.mygeofencing.R;


public class LocationBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(new Random().nextInt(), createNotification(intent.getExtras().get("coordinates").toString(), context));
    }

    // Create notification
    private Notification createNotification(String msg, Context context) {
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, "channel");
        notificationBuilder
                .setSmallIcon(R.drawable.cast_ic_notification_small_icon)
                .setColor(Color.RED)
                .setContentTitle(context.getString(R.string.msg_enter))
                .setContentText(msg)
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND)
                .setAutoCancel(true);
        return notificationBuilder.build();
    }
}
