package uhk.cz.mygeofencing.model;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class GeofenceRepository {

    Context context;
    final static String REPO_KEY = "repository";
    final static String ACC_KEY = "acc";
    final static String SH_KEY = "shares";
    public final static float RADIUS = 500; //in meters

    public GeofenceRepository(Context context) {
        this.context = context;
    }

    public void addGeofence (SimpleGeofence geofence) {
        SharedPreferences.Editor editor = context.getSharedPreferences(SH_KEY, Context.MODE_PRIVATE).edit();
        Set<String> geofences = getGeofeces();
        geofences.add(GeofenceUtil.encode(geofence));
        editor.putStringSet(REPO_KEY, geofences);
        editor.apply();
    }

    private Set<String> getGeofeces() {
        Set<String> result = new HashSet<>();
        SharedPreferences preferences = context.getSharedPreferences(SH_KEY, Context.MODE_PRIVATE);
        Set<String> set = preferences.getStringSet(REPO_KEY, null);

        if (set != null) {
            result = set;
        }

        return result;
    }

    public void removeGeofence(int i) {
        List<SimpleGeofence> geofences = getGeofences();
        geofences.remove(i);

        Set<String> set = new HashSet<>();
        for (SimpleGeofence g : geofences) {
            set.add(GeofenceUtil.encode(g));
        }

        clear();

        SharedPreferences.Editor editor = context.getSharedPreferences(SH_KEY, Context.MODE_PRIVATE).edit();
        editor.putStringSet(REPO_KEY, set);
        editor.apply();
    }

    public List<SimpleGeofence> getGeofences() {
        List<SimpleGeofence> list = new ArrayList<>();
        Set<String> set = getGeofeces();

        for (Iterator<String> it = set.iterator(); it.hasNext();) {
            String item = it.next();
            list.add(GeofenceUtil.decode(item));
        }

        return list;
    }

    public void clear() {
        SharedPreferences.Editor editor = context.getSharedPreferences(SH_KEY, Context.MODE_PRIVATE).edit();
        editor.remove(REPO_KEY);

        editor.apply();
    }

    public void setAccelerometer(boolean isChecked) {
        SharedPreferences.Editor editor = context.getSharedPreferences(SH_KEY, Context.MODE_PRIVATE).edit();
        editor.putBoolean(ACC_KEY, isChecked);
        editor.apply();
    }

    public boolean getAccelerometer() {
        return context.getSharedPreferences(SH_KEY, Context.MODE_PRIVATE).getBoolean(ACC_KEY, false);
    }
}
