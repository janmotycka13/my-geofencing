package uhk.cz.mygeofencing.model;

import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;

public class GeofenceUtil {

    final static String DELIMITER = ";";

    public static String encode(@NonNull final SimpleGeofence g) {
        return String.format("%s%s%s", String.valueOf(g.getLatitude()), DELIMITER, String.valueOf(g.getLongitude()));
    }

    public static SimpleGeofence decode(@NonNull final String s) {
        String[] parts = s.split(DELIMITER);
        double latitude = Double.parseDouble(parts[0]);
        double longitude = Double.parseDouble(parts[1]);
        float radius = GeofenceRepository.RADIUS;

        return new SimpleGeofence(latitude, longitude, radius);
    }

    public static boolean checkInside(SimpleGeofence geofence, LatLng lng) {
        return calcDistance(new LatLng(geofence.getLatitude(), geofence.getLongitude()), lng) < geofence.getRadius();
    }

    private static double calcDistance(LatLng lng1, LatLng lng2) {
        double c =
                Math.sin(Math.toRadians(lng1.latitude)) *
                        Math.sin(Math.toRadians(lng2.latitude)) +
                        Math.cos(Math.toRadians(lng1.latitude)) *
                                Math.cos(Math.toRadians(lng2.latitude)) *
                                Math.cos(Math.toRadians(lng2.longitude) -
                                        Math.toRadians(lng1.longitude));
        c = c > 0 ? Math.min(1, c) : Math.max(-1, c);

        return 3959 * 1.609 * 1000 * Math.acos(c);
    }

}
