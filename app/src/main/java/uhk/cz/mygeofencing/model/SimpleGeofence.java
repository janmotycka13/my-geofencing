package uhk.cz.mygeofencing.model;

public class SimpleGeofence {

    private double latitude;
    private double longitude;
    private float radius;

    public SimpleGeofence(double latitude, double longitude, float radius) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public float getRadius() {
        return radius;
    }

    @Override
    public String toString() {
        return String.format("Latitude: %s\nLongitude: %s\nRadius: %s", getLatitude(), getLongitude(), getRadius());
    }
}
