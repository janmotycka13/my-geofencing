package uhk.cz.mygeofencing.model;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import uhk.cz.mygeofencing.activities.MainActivity;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;


public class LocationService extends Service {
    private LocationListener listener;
    private LocationManager locationManager;
    private SensorManager sensorManager;
    private SensorEventListener sensorEventListener;
    private Sensor sensor;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

        listener = new GeofenceLocationListener(getApplicationContext());
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        //noinspection MissingPermission
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        if ((sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)) == null || !new GeofenceRepository(getApplicationContext()).getAccelerometer()) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MainActivity.REQUEST_LOCATION_UPDATES, 0, listener);
        } else {
            sensorEventListener = new SensorEventListener() {
                Float x, y, z;
                @Override
                public void onSensorChanged(SensorEvent sensorEvent) {
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) return;

                    if (x == null && y == null && z == null) {
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MainActivity.REQUEST_LOCATION_UPDATES, 0, listener);
                        x = sensorEvent.values[0];
                        y = sensorEvent.values[1];
                        z = sensorEvent.values[2];
                    } else {
                        if (x != sensorEvent.values[0] || y != sensorEvent.values[1] || z != sensorEvent.values[2]) {
                            x = sensorEvent.values[0];
                            y = sensorEvent.values[1];
                            z = sensorEvent.values[2];

                            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MainActivity.REQUEST_LOCATION_UPDATES, 0, listener);
                        } else {
                            locationManager.removeUpdates(listener);
                        }
                    }
                }

                @Override
                public void onAccuracyChanged(Sensor sensor, int i) {

                }
            };

            sensorManager.registerListener(sensorEventListener, sensor, SensorManager.SENSOR_DELAY_UI);

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(locationManager != null){
            //noinspection MissingPermission
            locationManager.removeUpdates(listener);
        }
    }
}
